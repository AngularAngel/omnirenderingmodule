
module omni.rendering {
    requires java.logging;
    requires static lombok;
    requires org.lwjgl;
    requires org.lwjgl.glfw;
    requires org.lwjgl.opengl;
    requires devil.util;
    requires trivial.json;
    requires osgi.core;
    requires omni.resource;
    requires omni.client;
    exports net.angle.omnirendering.api;
    exports net.angle.omnirendering.impl;
}