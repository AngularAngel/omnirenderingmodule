package net.angle.omnirendering.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.gl.Texture2D;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnirendering.api.TextureFrameBufferManager;
import static org.lwjgl.opengl.GL11C.GL_NEAREST;
import static org.lwjgl.opengl.GL11C.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11C.GL_TEXTURE_MIN_FILTER;
import org.lwjgl.opengl.GL30C;

/**
 *
 * @author angle
 */
public class BasicTextureFrameBufferManager extends BasicFrameBufferManager implements TextureFrameBufferManager {
    @RequiredArgsConstructor
    public class FBOTexture {
        private @Getter Texture2D texture;
        private final String uniformName;
        private final String fragDataLocationName;
        private final int fragDataLocation, bindingTarget, format, attachment;
        
        public Texture2D genTexture() {
            texture = DGL.genTex2D();
            texture.image(getWidth(), getHeight(), format);
            texture.bind();
            texture.parami(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            texture.parami(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            fbo.texture2D(texture, attachment);
            return texture;
        }
        
        public void bindFragDataLocation(ShaderProgram shader) {
            if (Objects.nonNull(fragDataLocationName))
                shader.bindFragDataLocation(fragDataLocationName, fragDataLocation);
        }
        
        public void bindTexture() {
            texture.bind(bindingTarget);
        }
        
        public void unbindTexture() {
            texture.unbind();
        }
        
        public void destroy() {
            DGL.delete(texture);
        }
        
        public void prepareShader(ShaderProgram shader) {
            shader.uniform1i(uniformName, bindingTarget - GL30C.GL_TEXTURE0);
        }
    }
    
    private final FBOTexture[] textures;
    
    public BasicTextureFrameBufferManager(int numTextures) {
        super();
        textures = new FBOTexture[numTextures];
    }
    
    public BasicTextureFrameBufferManager(String uniformName, String fragDataLocationName, int fragDataLocation, int bindingTarget, int format, int attachment) {
        this(1);
        textures[0] = new FBOTexture(uniformName, fragDataLocationName, fragDataLocation, bindingTarget, format, attachment);
    }
    
    public BasicTextureFrameBufferManager(String[] uniformNames, String[] fragDataLocationNames, int[] fragDataLocations, int[] bindingTargets, int[] formats, int[] attachments) {
        this(uniformNames.length);
        for (int i = 0; i < textures.length; i++)
            textures[i] = new FBOTexture(uniformNames[i], fragDataLocationNames[i], fragDataLocations[i], bindingTargets[i], formats[i], attachments[i]);
    }
    
    public BasicTextureFrameBufferManager(String uniformName, String fragDataLocationName, int fragDataLocation, int bindingTarget, int format, int attachment, int width, int height) {
        this(uniformName, fragDataLocationName, fragDataLocation, bindingTarget, format, attachment);
        resize(width, height);
    }

    @Override
    public Texture2D getTexture(int textureID) {
        return textures[textureID].getTexture();
    }
    
    public void clearTexture(Texture2D texture) {
        if (texture != null)
            DGL.delete(texture);
    }
    
    @Override
    public void genTextures() {
        DGL.bindFBO(fbo);
        for (FBOTexture fboTexture : textures)
            fboTexture.genTexture();
    }
    
    @Override
    public void bindFragDataLocations(ShaderProgram shader) {
        
    }
    
    @Override
    public void bindTextures() {
        for (FBOTexture fboTexture : textures)
            fboTexture.bindTexture();
    }
    
    @Override
    public void unbindTextures() {
        for (FBOTexture fboTexture : textures)
            fboTexture.unbindTexture();
    }
    
    @Override
    public void destroy() {
        super.destroy();
        for (FBOTexture fboTexture : textures)
            fboTexture.destroy();
    }

    @Override
    public void prepareShader(ShaderProgram shader) {
        for (FBOTexture fboTexture : textures)
            fboTexture.prepareShader(shader);
    }
}