package net.angle.omnirendering.impl;

import com.samrj.devil.gl.ShaderProgram;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import lombok.Getter;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.api.CallbackRenderingManager;

/**
 *
 * @author angle
 */
public class BasicCallbackRenderingManager implements CallbackRenderingManager {
    private final @Getter List<Runnable> initCallbacks = new ArrayList<>();
    private final @Getter List<Consumer<ShaderProgram>> renderCallbacks = new ArrayList<>();
    private final @Getter List<Consumer<Boolean>> destroyCallbacks = new ArrayList<>();

    public BasicCallbackRenderingManager(RenderComponent... renderComponents) {
        addRenderComponents(renderComponents);
    }
    
    @Override
    public void addInitCallback(Runnable initCallback) {
        initCallbacks.add(initCallback);
    }

    @Override
    public void addRenderCallback(Consumer<ShaderProgram> renderCallback) {
        renderCallbacks.add(renderCallback);
    }

    @Override
    public void addDestroyCallback(Consumer<Boolean> destroyCallback) {
        destroyCallbacks.add(destroyCallback);
    }

    @Override
    public void addRenderComponent(RenderComponent renderComponent) {
        addInitCallback(renderComponent::init);
        addRenderCallback(renderComponent::render);
        addDestroyCallback(renderComponent::destroy);
    }

    @Override
    public void insertInitCallback(Runnable initCallback, int index) {
        initCallbacks.add(index, initCallback);
    }

    @Override
    public void insertRenderCallback(Consumer<ShaderProgram> renderCallback, int index) {
        renderCallbacks.add(index, renderCallback);
    }

    @Override
    public void insertDestroyCallback(Consumer<Boolean> destroyCallback, int index) {
        destroyCallbacks.add(index, destroyCallback);
    }
}