/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omnirendering.impl;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.GLSLPreprocessor;
import com.samrj.devil.gl.Shader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import net.angle.omniresource.api.Resource;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.ResourceType;
import net.angle.omniresource.impl.AbstractResource;
import net.angle.omniresource.impl.BasicResourceType;
import org.apache.commons.io.IOUtils;
import org.lwjgl.opengl.GL32C;
import org.lwjgl.opengl.GL43C;

/**
 *
 * @author angle
 */
public class ShaderResource extends AbstractResource<Shader> {
    private static final String SHADER_PATH = "shaders/";
    
    public static final ResourceType<Resource<Shader>> TYPE = new BasicResourceType<>(ShaderResource::new, new String[]{"vert", "frag", "geo", "comp"});
    
    private static Map<URI, String> shaderSources;
    
    private final int type;
    private Shader shader;
    
    public ShaderResource(String name) {
        super(name);
        if (name.endsWith(".vert"))
            this.type = GL32C.GL_VERTEX_SHADER;
        else if (name.endsWith(".frag"))
            this.type = GL32C.GL_FRAGMENT_SHADER;
        else if (name.endsWith(".geo"))
            this.type = GL32C.GL_GEOMETRY_SHADER;
        else if (name.endsWith(".comp"))
            this.type = GL43C.GL_COMPUTE_SHADER;
        else throw new IllegalArgumentException("Shader must end in .vert, .geo, .frag, or .comp!");
    }
    
    public static void init() throws IOException {
        GLSLPreprocessor preprocessor = DGL.genGLSLPreprocessor();
        preprocessor.setLineDirectivesEnabled(false);
        for (Path filePath : Files.walk(Path.of(RESOURCE_PATH + SHADER_PATH)).collect(Collectors.toList()))
            if (Files.isRegularFile(filePath)) preprocessor.load(filePath.toUri());
        preprocessor.process();
        shaderSources = preprocessor.getSources();
        DGL.delete(preprocessor);
    }
    
    /**
     * @return the shader
     */
    @Override
    public Shader get() {
        if (!isLoaded()) throw new IllegalStateException();
        return shader;
    }

    @Override
    public InputStream getInputStream(Class loadingClass) throws IOException {
        URI path = Path.of(RESOURCE_PATH + SHADER_PATH).resolve(getName()).toUri();
        String source = shaderSources.get(path);
        if (Objects.nonNull(source))
            return IOUtils.toInputStream(source, "UTF-8");
        else
            return super.getInputStream(loadingClass);
    }

    @Override
    public void load(ResourceManager resourceManager, Class loadingClass) throws IOException {
        shader = DGL.genShader(type).source(getInputStream(loadingClass));
    }
    
    @Override
    public void unload(ResourceManager resourceManager) {
        if (!isLoaded()) throw new IllegalStateException();
        DGL.delete(shader);
        shader = null;
    }
    
    @Override
    public boolean isLoaded() {
        return shader != null;
    }
    
    @Override
    public String getResourcePath() {
        return RESOURCE_PATH + SHADER_PATH + getName();
    }

    @Override
    public ResourceType<Resource<Shader>> getType() {
        return TYPE;
    }
}