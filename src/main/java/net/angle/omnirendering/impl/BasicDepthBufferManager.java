/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnirendering.impl;

import static org.lwjgl.opengl.GL14C.GL_DEPTH_COMPONENT32;
import static org.lwjgl.opengl.GL30C.GL_DEPTH_ATTACHMENT;

/**
 *
 * @author angle
 */
public class BasicDepthBufferManager extends BasicTextureFrameBufferManager {
    
    public BasicDepthBufferManager(int bindingTarget) {
        super("u_depth_tex", null, -1, bindingTarget, GL_DEPTH_COMPONENT32, GL_DEPTH_ATTACHMENT);
    }
    
    public BasicDepthBufferManager(int width, int height, int bindingTarget) {
        this(bindingTarget);
        resize(width, height);
    }
}