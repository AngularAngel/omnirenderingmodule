package net.angle.omnirendering.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.ShaderProgram;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnirendering.api.ShaderProgramManager;
import net.angle.omniresource.api.ResourceManager;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicShaderProgramManager implements ShaderProgramManager {
    private final @Getter String name;
    private final ResourceManager resourceManager;
    private @Getter ShaderProgram shader;
    
    
    @Override
    public void init() {
        try {
            shader = resourceManager.load(ShaderProgramResource.TYPE, getName(), this);
        } catch (IOException ex) {
            Logger.getLogger(BasicShaderProgramManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void destroy(Boolean crashed) {
        resourceManager.unloadResource(getName(), this);
    }
}