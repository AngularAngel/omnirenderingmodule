/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnirendering.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.VertexStream;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import net.angle.omnirendering.api.TextureVertexManager3D;

/**
 *
 * @author angle
 */
public class AllocatedTextureVertexManager3D extends AbstractVertexManager implements TextureVertexManager3D {
    protected Vec3 bufferVPos;
    protected Vec2 bufferVTexCoord;

    @Override
    public VertexStream getVertexBuilder() {
        return (VertexStream) super.getVertexBuilder();
    }
    
    @Override
    public void beginVariables(int vertices, int indices) {
        vertexBuilder = DGL.genVertexStream(vertices, indices);
        
        bufferVPos = vertexBuilder.vec3("in_pos");
        bufferVTexCoord = vertexBuilder.vec2("in_tex_coord");
    }
    
    @Override
    public void vertex(Vec3 vPos, Vec2 vTexCoord) {
        bufferVPos.set(vPos); 
        bufferVTexCoord.set(vTexCoord);
        vertexBuilder.vertex();
    }
    
    @Override
    public void uploadVertices() {
        getVertexBuilder().upload();
    }
}