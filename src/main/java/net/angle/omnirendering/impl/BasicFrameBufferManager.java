/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnirendering.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.FBO;
import lombok.Getter;
import lombok.Setter;
import net.angle.omnirendering.api.FrameBufferManager;
import static org.lwjgl.opengl.GL30C.GL_READ_FRAMEBUFFER;

/**
 *
 * @author angle
 */
public class BasicFrameBufferManager implements FrameBufferManager {
    
    private @Getter @Setter int width, height;
    
    protected FBO fbo;
    
    public BasicFrameBufferManager() {
        fbo = DGL.genFBO();
        DGL.bindFBO(fbo);
    }
    
    public BasicFrameBufferManager(int width, int height) {
        this();
        resize(width, height);
    }

    @Override
    public FBO getFBO() {
        return fbo;
    }
    
    @Override
    public void drawToFBO() {
        DGL.bindFBO(fbo);
    }
    
    @Override
    public void readFBO() {
        DGL.bindFBO(fbo, GL_READ_FRAMEBUFFER);
    }
    
    @Override
    public void destroy() {
        DGL.delete(fbo);
    }
}