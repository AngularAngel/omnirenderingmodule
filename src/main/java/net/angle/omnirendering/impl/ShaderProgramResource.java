/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omnirendering.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.Shader;
import com.samrj.devil.gl.ShaderProgram;
import java.io.IOException;
import net.angle.omniresource.api.Resource;
import org.lwjgl.opengl.GL32C;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.ResourceType;
import net.angle.omniresource.impl.AbstractResource;
import net.angle.omniresource.impl.JSONResource;
import net.angle.omniresource.impl.BasicResourceType;
import org.lwjgl.opengl.GL43C;
import pixelguys.json.JsonArray;
import pixelguys.json.JsonObject;
import pixelguys.json.JsonString;

/**
 *
 * @author angle
 */
public class ShaderProgramResource extends AbstractResource<ShaderProgram> {
    
    public static final ResourceType<Resource<ShaderProgram>> TYPE = new BasicResourceType<>(ShaderProgramResource::new, new String[]{});
    
    private ShaderProgram shaderProgram;
    
    protected ShaderProgramResource(String name) {
        super(name);
    }
    
    protected String propertiesFileName() {
        return getName() + "_properties.json";
    }
    
    protected int[] getComponents(ResourceManager resourceManager, Class clazz) {
        try {
            Resource<JsonObject> jsonResource = resourceManager.loadResource(JSONResource.TYPE, propertiesFileName(), this, clazz);
            JsonObject json = jsonResource.get();
             
            JsonArray shaders = json.getArray("shaders");
            int[] components = new int[shaders.array.size()];
            for (int i = 0; i < components.length; i++) {
                switch(((JsonString) shaders.array.get(i)).value) {
                    case "compute" -> components[i] = GL43C.GL_COMPUTE_SHADER;
                    case "vertex" -> components[i] = GL32C.GL_VERTEX_SHADER;
                    case "fragment" -> components[i] = GL32C.GL_FRAGMENT_SHADER;
                    case "geometry" -> components[i] = GL32C.GL_GEOMETRY_SHADER;
                }
            }
            resourceManager.unloadResource(propertiesFileName(), this);
            return components;
        } catch (IOException ex) {
            return new int[] {GL32C.GL_VERTEX_SHADER, GL32C.GL_FRAGMENT_SHADER};
        }
    }

    @Override
    public void load(ResourceManager resourceManager, Class loadingClass) throws IOException {
        int[] components = getComponents(resourceManager, loadingClass);
        
        Resource<Shader>[] shaderResources = new ShaderResource[components.length];
        Shader[] shaders = new Shader[components.length];
        
        for (int i = 0; i < components.length; i++) {
            int component = components[i];
            String name = getName();
            switch(component) {
                case GL32C.GL_VERTEX_SHADER -> name = name + ".vert";
                case GL32C.GL_FRAGMENT_SHADER -> name = name + ".frag";
                case GL32C.GL_GEOMETRY_SHADER -> name = name + ".geo";
                case GL43C.GL_COMPUTE_SHADER -> name = name + ".comp";
                default -> throw new IOException("Unknown shader component!");
            }
            shaderResources[i] = resourceManager.loadResource(ShaderResource.TYPE, name, this, loadingClass);
            shaders[i] = shaderResources[i].get();
        }
        
        shaderProgram = DGL.loadProgram(shaders);
        for (Resource<Shader> shaderResource : shaderResources)
            resourceManager.unloadResource(shaderResource.getName(), this);
    }

    /**
     * @return the shader
     */
    @Override
    public ShaderProgram get() {
        if (!isLoaded()) throw new IllegalStateException();
        return shaderProgram;
    }

    @Override
    public void unload(ResourceManager resourceManager) {
        if (!isLoaded()) throw new IllegalStateException();
        DGL.delete(shaderProgram);
        shaderProgram = null;
    }

    @Override
    public boolean isLoaded() {
        return shaderProgram != null;
    }

    @Override
    public String getResourcePath() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public ResourceType<Resource<ShaderProgram>> getType() {
        return TYPE;
    }
}