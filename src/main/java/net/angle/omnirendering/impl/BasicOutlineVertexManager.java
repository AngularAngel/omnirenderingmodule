/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnirendering.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.VertexBuffer;
import com.samrj.devil.gl.VertexStream;
import com.samrj.devil.math.Vec3;
import net.angle.omnirendering.api.OutlineVertexManager;

/**
 *
 * @author angle
 */
public class BasicOutlineVertexManager extends AbstractVertexManager implements OutlineVertexManager {
    private Vec3 outlineVPos;
    
    @Override
    public VertexStream getVertexBuilder() {
        return (VertexStream) super.getVertexBuilder();
    }
    
    @Override
    public void beginVariables(int vertices, int indices) {
        vertexBuilder = DGL.genVertexStream(vertices, indices);
        outlineVPos = vertexBuilder.vec3("in_pos");
    }

    @Override
    public void streamIndex(int index) {
        vertexBuilder.index(index);
    }
    
    @Override
    public void streamVertex(Vec3 vertex) {
        outlineVPos.set(vertex);
        vertexBuilder.vertex();
    }

    @Override
    public void uploadVertices() {
        getVertexBuilder().upload();
    }
}