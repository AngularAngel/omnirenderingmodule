package net.angle.omnirendering.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.VertexBuilder;
import lombok.Getter;
import net.angle.omnirendering.api.VertexManager;

/**
 *
 * @author angle
 */
public abstract class AbstractVertexManager implements VertexManager {
    protected @Getter VertexBuilder vertexBuilder;

    @Override
    public void begin() {
        getVertexBuilder().begin();
    }

    @Override
    public int getNumVertices() {
        return getVertexBuilder().numVertices();
    }

    @Override
    public int getNumIndices() {
        return getVertexBuilder().numIndices();
    }

    @Override
    public void drawVertices(int mode) {
        DGL.draw(getVertexBuilder(), mode);
    }

    @Override
    public void deleteVertices() {
        if (vertexBuilder != null)
        DGL.delete(vertexBuilder);
    }
    
}