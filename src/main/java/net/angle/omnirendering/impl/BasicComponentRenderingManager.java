package net.angle.omnirendering.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import net.angle.omnirendering.api.ComponentRenderingManager;
import net.angle.omnirendering.api.RenderComponent;

/**
 *
 * @author angle
 */
public class BasicComponentRenderingManager implements ComponentRenderingManager {
    private final @Getter List<RenderComponent> renderComponents;

    public BasicComponentRenderingManager(RenderComponent... renderComponents) {
        this.renderComponents = new ArrayList<>(Arrays.asList(renderComponents));
    }
    
    @Override
    public void addRenderComponent(RenderComponent renderComponent) {
        renderComponents.add(renderComponent);
    }
}