/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnirendering.impl;

import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.angle.omnirendering.api.OutlineVertexManager;
import net.angle.omnirendering.api.VertexShape;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
@NoArgsConstructor
public class Icosahedron implements VertexShape {
    
    private final float X = 0.525731112119133606f;
    private final float Z = 0.850650808352039932f;
    private final float N = 0.f;
    
    private final @Getter Vec3[] vertices = new Vec3[]{new Vec3(-X,N,Z), new Vec3(X,N,Z), new Vec3(-X,N,-Z), new Vec3(X,N,-Z), 
        new Vec3(N,Z,X), new Vec3(N,Z,-X), new Vec3(N,-Z,X), new Vec3(N,-Z,-X), new Vec3(Z,X,N), new Vec3(-Z,X,N), 
        new Vec3(Z,-X,N), new Vec3(-Z,-X,N)};
    
    private final @Getter Vec3i[] triangles = new Vec3i[]{new Vec3i(0,4,1), new Vec3i(0,9,4), new Vec3i(9,5,4), new Vec3i(4,5,8), 
        new Vec3i(4,8,1), new Vec3i(8,10,1), new Vec3i(8,3,10), new Vec3i(5,3,8), new Vec3i(5,2,3), new Vec3i(2,7,3), 
        new Vec3i(7,10,3), new Vec3i(7,6,10), new Vec3i(7,11,6), new Vec3i(11,0,6), new Vec3i(0,1,6), new Vec3i(6,1,10), 
        new Vec3i(9,0,11), new Vec3i(9,11,2), new Vec3i(9,2,5), new Vec3i(7,2,11)};
    
    private final OutlineVertexManager outline = new BasicOutlineVertexManager();
    
    @Override
    public void init() {
        initOutline();
    }
    
    private void initOutline() {
        outline.beginVariables(12, 60);
        outline.begin();
        outline.streamPolygonOutline(vertices, triangles);
        outline.upload();
    }

    @Override
    public void render(ShaderProgram shader) {
        outline.draw(GL11C.GL_TRIANGLES);
    }

    @Override
    public void render() {
        outline.draw(GL11C.GL_TRIANGLES);
    }

    @Override
    public void destroy(Boolean crashed) {
        outline.destroy();
    }
}