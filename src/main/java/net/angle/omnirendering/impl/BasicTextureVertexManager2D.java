/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnirendering.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.VertexBuffer;
import com.samrj.devil.math.Vec2;
import net.angle.omnirendering.api.TextureVertexManager2D;

/**
 *
 * @author angle
 */
public class BasicTextureVertexManager2D extends AbstractVertexManager implements TextureVertexManager2D {
    protected Vec2 bufferVPos;
    protected Vec2 bufferVTexCoord;

    @Override
    public void beginVariables(int vertices, int indices) {
        vertexBuilder = DGL.genVertexBuffer(vertices, indices);
        
        bufferVPos = vertexBuilder.vec2("in_pos");
        bufferVTexCoord = vertexBuilder.vec2("in_tex_coord");
    }

    @Override
    public VertexBuffer getVertexBuilder() {
        return (VertexBuffer) super.getVertexBuilder();
    }
    
    @Override
    public void vertex(Vec2 vPos, Vec2 vTex) {
        bufferVPos.set(vPos);
        bufferVTexCoord.set(vTex);
        vertexBuilder.vertex();
    }
    
    @Override
    public void uploadVertices() {
        getVertexBuilder().end();
    }
}