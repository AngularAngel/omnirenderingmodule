package net.angle.omnirendering.api;

import com.samrj.devil.gl.ShaderProgram;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author angle
 */
public interface CallbackRenderingManager extends RenderingManager {
    public List<Runnable> getInitCallbacks();
    public List<Consumer<ShaderProgram>> getRenderCallbacks();
    public List<Consumer<Boolean>> getDestroyCallbacks();
    
    public void insertInitCallback(Runnable initCallback, int index);
    public void insertRenderCallback(Consumer<ShaderProgram> renderCallback, int index);
    public void insertDestroyCallback(Consumer<Boolean> destroyCallback, int index);
    
    public void addInitCallback(Runnable initCallback);
    public void addRenderCallback(Consumer<ShaderProgram> renderCallback);
    public void addDestroyCallback(Consumer<Boolean> destroyCallback);
    
    public default void addInitCallbacks(Runnable... initCallbacks) {
        for (Runnable initCallback : initCallbacks)
            addInitCallback(initCallback);
    }
    
    public default void addRenderCallbacks(Consumer<ShaderProgram>... renderCallbacks) {
        for (Consumer<ShaderProgram> renderCallback : renderCallbacks)
            addRenderCallback(renderCallback);
    }
    
    public default void addDestroyCallbacks(Consumer<Boolean>... destroyCallbacks) {
        for (Consumer<Boolean> destroyCallback : destroyCallbacks)
            addDestroyCallback(destroyCallback);
    }
    
    @Override
    public default void init() {
        for (Runnable initCallback : getInitCallbacks())
            initCallback.run();
    }
    
    @Override
    public default void render() {
        throw new UnsupportedOperationException("Must specify ShaderProgram for render call!");
    }
    
    @Override
    public default void render(ShaderProgram shader) {
        for (Consumer<ShaderProgram> renderCallback : getRenderCallbacks())
            renderCallback.accept(shader);
    }
    
    @Override
    public default void destroy(Boolean crashed) {
        for (Consumer<Boolean> destroyCallback : getDestroyCallbacks())
            destroyCallback.accept(crashed);
    }
}