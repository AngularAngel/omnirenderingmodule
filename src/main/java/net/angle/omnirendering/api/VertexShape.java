package net.angle.omnirendering.api;

import com.samrj.devil.math.Vec3;

/**
 *
 * @author angle
 */
public interface VertexShape extends RenderComponent {
    public Vec3[] getVertices();
}