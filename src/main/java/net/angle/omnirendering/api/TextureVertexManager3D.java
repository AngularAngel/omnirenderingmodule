package net.angle.omnirendering.api;

import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public interface TextureVertexManager3D extends VertexManager {
    public void vertex(Vec3 vPos, Vec2 vTexCoord);
    
    public default void bufferQuad(Vec3 topLeft, Vec3 topRight, Vec3 bottomRight, Vec3 bottomLeft) {
        bufferQuad(topLeft, topRight, bottomRight, bottomLeft, 1, 1);
    }
    
    public default void bufferQuad(Vec3 topLeft, Vec3 topRight, Vec3 bottomRight, Vec3 bottomLeft, int width, int height) {
        int startingVertex = getNextVertex();
        vertex(topLeft, new Vec2(0, width));
        vertex(topRight, new Vec2(height, width));
        vertex(bottomRight, new Vec2(height, 0));
        vertex(bottomLeft, new Vec2(0, 0));
        
        index(startingVertex);
        index(startingVertex + 1);
        index(startingVertex + 2);
        index(startingVertex);
        index(startingVertex + 2);
        index(startingVertex + 3);
    }
    
    public default void bufferFlatVertices(float startx, float starty, float startz, float xoff, float yoff, float zoff) {
        //Build a square out of two triangles.

        Vec3 topLeft, topRight, bottomLeft, bottomRight;
        
        int width, height;

        topLeft = new Vec3(0, 0, 0);

        if (xoff == 0) {
            topRight = new Vec3(0, 0, zoff);
            width = (int) zoff;
        } else {
            topRight = new Vec3(xoff, 0, 0);
            width = (int) xoff;
        }

        bottomRight = new Vec3(xoff, yoff, zoff);

        if (yoff == 0) {
            bottomLeft = new Vec3(0, 0, zoff);
            height = (int) zoff;
        } else{
            bottomLeft = new Vec3(0, yoff, 0);
            height = (int) yoff;
        }
        
        //adjust positions for where our starts are.
        topLeft.add(new Vec3(startx, starty, startz));
        topRight.add(new Vec3(startx, starty, startz));
        bottomLeft.add(new Vec3(startx, starty, startz));
        bottomRight.add(new Vec3(startx, starty, startz));
        
        bufferQuad(topLeft, topRight, bottomRight, bottomLeft, width, height);
    }

    @Override
    public default void drawVertices() {
        drawVertices(GL11C.GL_TRIANGLES);
    }
}