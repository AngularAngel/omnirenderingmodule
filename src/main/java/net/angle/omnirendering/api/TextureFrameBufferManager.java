package net.angle.omnirendering.api;

import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.gl.Texture2D;

/**
 *
 * @author angle
 */
public interface TextureFrameBufferManager extends FrameBufferManager {
    public void genTextures();
    public void bindFragDataLocations(ShaderProgram shader);
    public void bindTextures();
    public void unbindTextures();
    public void prepareShader(ShaderProgram shader);
    
    @Override
    public default void resize(int width, int height) {
        FrameBufferManager.super.resize(width, height);
        
        genTextures();
    }
    
    public Texture2D getTexture(int textureID);
    public default Texture2D getTexture() {
        return getTexture(0);
    }
}