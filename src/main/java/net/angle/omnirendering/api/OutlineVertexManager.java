package net.angle.omnirendering.api;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public interface OutlineVertexManager extends VertexManager {
    public void streamVertex(Vec3 vertex);
    public void streamIndex(int index);
    
    public default void streamVertices(Vec3[] vertices) {
        for (Vec3 vertex : vertices)
            streamVertex(vertex);
    }
    
    public default void streamTriangleIndices(Vec3i triangle) {
        streamIndex(triangle.x);
        streamIndex(triangle.y);
        streamIndex(triangle.z);
    }
    
    public default void streamTriangleIndices(Vec3i[] triangles) {
        for (Vec3i triangle : triangles) {
            streamTriangleIndices(triangle);
        }
    }
    
    public default void streamPolygonOutline(Vec3[] vertices, Vec3i[] triangles)  {
        streamVertices(vertices);
        streamTriangleIndices(triangles);
    }
    
    
    public default void streamCubeOutline(Vec3 coord, float sideLength) {
        int index = getNextIndex();
        //starting corner
        float startx = (coord.x);
        float starty = (coord.y);
        float startz = (coord.z);
        
        //end corner
        float endx = startx + sideLength;
        float endy = starty + sideLength;
        float endz = startz + sideLength;
        
        
        streamVertex(new Vec3(startx, starty, startz));
        
        streamVertex(new Vec3(endx, starty, startz));
        streamVertex(new Vec3(startx, endy, startz));
        streamVertex(new Vec3(startx, starty, endz));
        
        streamVertex(new Vec3(endx, endy, startz));
        streamVertex(new Vec3(startx, endy, endz));
        streamVertex(new Vec3(endx, starty, endz));
        
        streamVertex(new Vec3(endx, endy, endz));
        
        streamIndex(index);
        streamIndex(index + 1);
        
        streamIndex(index);
        streamIndex(index + 2);
        
        streamIndex(index);
        streamIndex(index + 3);
        
        streamIndex(index + 1);
        streamIndex(index + 4);
        
        streamIndex(index + 2);
        streamIndex(index + 5);
        
        streamIndex(index + 3);
        streamIndex(index + 6);
        
        streamIndex(index + 1);
        streamIndex(index + 6);
        
        streamIndex(index + 2);
        streamIndex(index + 4);
        
        streamIndex(index + 3);
        streamIndex(index + 5);
        
        streamIndex(index + 4);
        streamIndex(index + 7);
        
        streamIndex(index + 5);
        streamIndex(index + 7);
        
        streamIndex(index + 6);
        streamIndex(index + 7);
    }
    
    public default void streamCubeFaces(Vec3 coord, float sideLength) {
        int index = getNextIndex();
        //starting corner
        float startx = (coord.x);
        float starty = (coord.y);
        float startz = (coord.z);
        
        //end corner
        float endx = startx + sideLength;
        float endy = starty + sideLength;
        float endz = startz + sideLength;
        
        
        streamVertex(new Vec3(startx, starty, startz));
        
        streamVertex(new Vec3(endx, starty, startz));
        streamVertex(new Vec3(startx, endy, startz));
        streamVertex(new Vec3(startx, starty, endz));
        
        streamVertex(new Vec3(endx, endy, startz));
        streamVertex(new Vec3(startx, endy, endz));
        streamVertex(new Vec3(endx, starty, endz));
        
        streamVertex(new Vec3(endx, endy, endz));
        
        //front
        streamIndex(index);
        streamIndex(index + 1);
        streamIndex(index + 4);
        
        streamIndex(index);
        streamIndex(index + 4);
        streamIndex(index + 2);
        
        //top
        streamIndex(index + 3);
        streamIndex(index + 6);
        streamIndex(index + 1);
        
        streamIndex(index + 3);
        streamIndex(index + 1);
        streamIndex(index + 0);
        
        //left
        streamIndex(index + 3);
        streamIndex(index);
        streamIndex(index + 2);
        
        streamIndex(index + 3);
        streamIndex(index + 2);
        streamIndex(index + 5);
        
        //back
        streamIndex(index + 6);
        streamIndex(index + 3);
        streamIndex(index + 5);
        
        streamIndex(index + 6);
        streamIndex(index + 5);
        streamIndex(index + 7);
        
        //bottom
        streamIndex(index + 2);
        streamIndex(index + 4);
        streamIndex(index + 7);
        
        streamIndex(index + 2);
        streamIndex(index + 7);
        streamIndex(index + 5);
        
        //right
        streamIndex(index + 1);
        streamIndex(index + 6);
        streamIndex(index + 7);
        
        streamIndex(index + 1);
        streamIndex(index + 7);
        streamIndex(index + 4);
    }
    
    public default void streamCubeOutline(Vec3 coord) {
        streamCubeOutline(coord, 1);
    }
    
    @Override
    public default void draw() {
        draw(GL11C.GL_TRIANGLES);
    }

    @Override
    public default void drawVertices() {
        drawVertices(GL11C.GL_TRIANGLES);
    }
}