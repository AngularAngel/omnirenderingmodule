package net.angle.omnirendering.api;

/**
 *
 * @author angle
 */
public interface RenderTemplate {
    public RenderComponent getComponent(Object[] info);
}