package net.angle.omnirendering.api;

import com.samrj.devil.gl.FBO;

/**
 *
 * @author angle
 */
public interface FrameBufferManager {
    public int getWidth();
    public int getHeight();
    public void setWidth(int width);
    public void setHeight(int height);
    public default void resize(int width, int height) {
        setWidth(width);
        setHeight(height);
    }
    public void drawToFBO();
    public void readFBO();
    public void destroy();
    public FBO getFBO();
}