package net.angle.omnirendering.api;

/**
 *
 * @author angle
 */
public interface RenderingManager extends RenderComponent {
    public void addRenderComponent(RenderComponent renderComponent);
    public default void addRenderComponents(RenderComponent... renderComponents) {
        for (RenderComponent renderComponent : renderComponents)
            addRenderComponent(renderComponent);
    }
}