package net.angle.omnirendering.api;

import com.samrj.devil.gl.ShaderProgram;

/**
 *
 * @author angle
 */
public interface RenderComponent {
    public default void init() {};
    public default void render() {
        render(null);
    };
    public default void render(ShaderProgram shader) {};
    public default void destroy(Boolean crashed) {};
}