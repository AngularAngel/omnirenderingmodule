package net.angle.omnirendering.api;

import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.gl.VertexBuilder;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public interface VertexManager extends RenderComponent {
    public VertexBuilder getVertexBuilder();
    public void beginVariables(int vertices, int indices);
    public void begin();
    public default void index(int index) {
        getVertexBuilder().index(index);
    };
    public default void vertex() {
        getVertexBuilder().vertex();
    }
    public int getNumVertices();
    public int getNumIndices();
    public default int getNextVertex() {
        return getNumVertices();
    }
    public default int getNextIndex() {
        return getNumIndices();
    }
    public void uploadVertices();
    public void deleteVertices();
    public void drawVertices();
    public void drawVertices(int mode);
    
    public default void upload() {
        uploadVertices();
    }
    
    public default void draw() {
        draw(GL11C.GL_TRIANGLES);
    }

    @Override
    public default void render(ShaderProgram shader) {
        draw();
    }

    @Override
    public default void render() {
        draw();
    }
    
    public default void draw(int mode) {
        drawVertices(mode);
    }
    
    public default void destroy() {
        deleteVertices();
    }

    @Override
    public default void destroy(Boolean crashed) {
        destroy();
    }
}