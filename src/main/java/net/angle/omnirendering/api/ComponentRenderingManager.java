package net.angle.omnirendering.api;

import com.samrj.devil.gl.ShaderProgram;
import java.util.List;

/**
 *
 * @author angle
 */
public interface ComponentRenderingManager extends RenderingManager {
    public List<RenderComponent> getRenderComponents();
    
    @Override
    public default void init() {
        for (RenderComponent renderComponent : getRenderComponents())
            renderComponent.init();
    }
    
    @Override
    public default void render() {
        for (RenderComponent renderComponent : getRenderComponents())
            renderComponent.render();
    }
    
    @Override
    public default void render(ShaderProgram shader) {
        for (RenderComponent renderComponent : getRenderComponents())
            renderComponent.render(shader);
    }
    
    @Override
    public default void destroy(Boolean crashed) {
        for (RenderComponent renderComponent : getRenderComponents())
            renderComponent.destroy(crashed);
    }
}