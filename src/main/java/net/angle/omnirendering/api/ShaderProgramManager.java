package net.angle.omnirendering.api;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.ShaderProgram;

/**
 *
 * @author angle
 */
public interface ShaderProgramManager extends RenderComponent {
    
    public String getName();
    public ShaderProgram getShader();
    
    @Override
    public default void render() {
        DGL.useProgram(getShader());
    }
}