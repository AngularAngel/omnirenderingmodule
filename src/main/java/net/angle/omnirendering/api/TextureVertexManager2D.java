package net.angle.omnirendering.api;

import com.samrj.devil.math.Vec2;
import static org.lwjgl.opengl.GL11C.GL_TRIANGLES;

/**
 *
 * @author angle
 */
public interface TextureVertexManager2D extends VertexManager {
    public void vertex(Vec2 vPos, Vec2 vTex);
    
    public default void bufferVertices(Vec2 topLeft, Vec2 bottomRight) {
        int startingVertex = getNextVertex();
        Vec2 topRight = new Vec2(bottomRight.x, topLeft.y),
             bottomLeft = new Vec2(topLeft.x, bottomRight.y);
        
        vertex(topLeft, new Vec2(0, 1));
        vertex(topRight, new Vec2(1, 1));
        vertex(bottomRight, new Vec2(1, 0));
        vertex(bottomLeft, new Vec2(0, 0));
        
        index(startingVertex);
        index(startingVertex + 1);
        index(startingVertex + 2);
        index(startingVertex);
        index(startingVertex + 2);
        index(startingVertex + 3);
    }

    @Override
    public default void drawVertices() {
        drawVertices(GL_TRIANGLES);
    }
}